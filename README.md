# go-webserver-demo

Demo project for a go webserver.

To start, simply use `go run .`

```
➜  ~ curl http://localhost:8080
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Demo</title>
    </head>
    <body>
        <h1>Hello World</h1>
    </body>
</html>
➜  ~
```
