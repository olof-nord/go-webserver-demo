package main

import (
    "gitlab.com/olof-nord/go-webserver-demo/webserver"
)

func main() {
    webserver.Run();
}
