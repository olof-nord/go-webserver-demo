package webserver

import (
    "net/http"
    "log"
    "os"
)

func Run() error {
	log.Printf("Starting webserver")
	webserver()
	return nil
}

func webserver() {
	for {
		defer log.Fatal("Webserver error")

		sitePath := "/site"
		currentPath, err := os.Getwd()

		if err != nil {
            log.Fatal(err)
        }

        sitePath = currentPath + sitePath

		log.Println("Using website path", sitePath)

		http.Handle("/", http.FileServer(http.Dir(sitePath)))

		log.Fatal("Webserver error", http.ListenAndServe(":8080", nil))
	}
}
